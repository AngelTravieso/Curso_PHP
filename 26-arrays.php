<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP</title>
</head>
<body>

<?php

    // Array Indexado
    // $semana[] = "Lunes";
    // $semana[] = "Martes";
    // $semana[] = "Miercoles";
    // $semana[] = "Jueves";

    // count cuenta los elementos en un array
    // for ($i = 0; $i < count($semana); $i++) {
    //    echo  $semana[$i] . "<br>";
    // }

    // Agregar al final del Array numerico
    // $semana[] = "Viernes";
    
    // for ($i = 0; $i < count($semana); $i++) {
    //     echo  $semana[$i] . "<br>";
    //  }

    
    // $semana = array("Lunes", "Martes", "Miercoles", "Jueves");
    // echo $semana[3];

    // Ordenar arreglo alfabeticamente
    // sort($semana);

    // for ($i = 0; $i < count($semana); $i++) {
    //     echo $semana[$i] . "<br>";
    // }

    // Array Asociativos
    // $datos = array("Nombre" => "Angel", "Apellido" => "Travieso", "Edad" => 20);
    // echo $datos["Apellido"]; 

    //$datos = "Martin";

    // is_array verifica si el dato en cuestion es un arreglo
    // if (is_array($datos)) {
    //     echo "Es un Array";
    // } else {
    //     echo "No es un Array";
    // }

    // $datos = array("Nombre" => "Angel", "Apellido" => "Travieso", "Edad" => 20);
    
    // Agregar elemento al final del Array asociativo
    // $datos["Pais"] = "Venezuela";
    
    // Recorrer array asociativo con forEach
    // foreach ($datos as $clave => $valor) {
    //     echo "A $clave le corresponde $valor <br>";
    // }
    






?>
    
</body>
</html>