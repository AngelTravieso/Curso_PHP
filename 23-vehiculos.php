<?php

	class Coche {

		protected $ruedas;
		var $color;
		protected $motor;

		// Metodo constructor
		function __construct() {
			$this -> ruedas = 4;
			$this -> color = "";
			$this -> motor = 1600;
		}

		function getRuedas() {
			return $this -> ruedas;
		}

		function getMotor() {
			return $this -> motor;
		}

		function arrancar() {
			echo "Estoy arrancando<br>";
		}

		function girar() {
			echo "Estoy girando<br>";
		}

		function frenar() {
			echo "Estoy frenando<br>";
		}

		function setColor($color, $nombre) {
			$this -> color = $color;

			echo sprintf("El color del %s es: %s <br>", $nombre, $color);
		}
	}

	// Estado inicial del objeto o instancia
	// $renault = new Coche();
	// $mazda = new Coche();
	// $seat = new Coche();

	// $renault -> estableceColor("Rojo", "Renault");
	// $seat -> estableceColor("Azul", "Seat");

	// $mazda -> girar();
	// echo $mazda -> ruedas;

// -----------------------------------------------------------------------------

	class Camion extends Coche {
		function __construct() {
			$this -> ruedas = 8;
			$this -> color = "gris";
			$this -> motor = 2600;
		}

		function arrancar() {
			// parent ejecuta el codigo de la clase padre
			parent::arrancar();

			echo "Camión arrancado...<br>";
		}

	}
?>