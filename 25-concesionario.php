<?php

	class Compra_vehiculo {
		
		private $precioBase;
		private static $ayuda = 0;
				 
		function __construct($gama) {
			
			if ($gama === "urbano") {
				
				$this -> precioBase = 10000;
				
			} else if ($gama === "compacto") {
				
				$this -> precioBase = 20000;	
				
			} else if ($gama === "berlina") {
				
				$this -> precioBase = 30000;	
				
			}		
		}

		static function descuentoGobierno() {

			if (date("m-d-y") > "05-01-20") {
				self::$ayuda = 4500;
			}

		}
		
		
		function climatizador() {		
			
			$this -> precioBase += 2000;					
			
		}
		
		
		function navegadorGps(){
			
			$this -> precioBase += 2500;	
			
		}
		

		function tapiceriaCuero($color){
			
			if ($color === "blanco") {
			
				$this -> precioBase += 3000;

			} else if ($color === "beige") {
				
				$this -> precioBase += 3500;
				
			} else {
				
				$this -> precioBase += 5000;
				
			}
		}
		
		
		function precioFinal(){
			
			$valorFinal = $this -> precioBase - self::$ayuda;
			return $valorFinal . "<br>";
			
		}
	}
?>