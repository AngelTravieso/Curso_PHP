<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP</title>
</head>
<body>

<?php

    $alimentos = array("fruta" => array("tropical" => "kiwi",
                                        "citrico" => "mandarina",
                                        "otros" => "manzana"),

                       "leche" => array("animal" => "vaca",
                                        "vegetal" => "coco"), 

                       "carne" => array("vacuno" => "lomo",
                                        "porcino" => "pata"));

                                        
    // echo $alimentos["carne"]["vacuno"];

    // Recorrer array de 2 dimensiones
    // foreach ($alimentos as $claveAlim => $alim) {
    //     echo "$claveAlim:<br>";

    //     foreach($alim as $valor => $elemento) {
    //         echo "$valor: $elemento <br>";
    //     }

    //     echo "<br>";
    // }

    echo var_dump($alimentos);

?>
    
</body>
</html>