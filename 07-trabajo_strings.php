<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>PHP</title>
	<style>
		.resaltar {
			color: red;
			font-weight: bold;
		}
	</style>
</head>
<body>
	<?php

		$variable1 = "casa";
		$variable2 = "CASA";

		// strcasecmp ignora mayusculas
		// strcmp compara teniendo en cuenta si estan o no en mayusculas
		// Devuelve 1 si no son iguales y 0 si lo son
		$resultado = strcmp($variable1, $variable2); 

		// echo "El resultado es $resultado";

		if (!$resultado) {
			echo "Coinciden";
		} else {
			echo "No coinciden";
		}
	?>
	
</body>
</html>